package zhexian.app.zoschina.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.view.View;

import zhexian.app.zoschina.R;
import zhexian.app.zoschina.base.BaseActionBarActivity;
import zhexian.app.zoschina.lib.ZDisplay;
import zhexian.app.zoschina.lib.ZImage;
import zhexian.app.zoschina.news.NewsListFragment;


public class MainActivity extends BaseActionBarActivity implements INavigatorCallback {
    private DrawerLayout mDrawerLayout;
    private View mNavigatorView;
    private boolean mIsNightMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ZImage.Init(this);
        mIsNightMode = getMyApplication().isNightMode();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_drawerLayout);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mNavigatorView = findViewById(R.id.main_navigator);

        NavigatorFragment navigatorFragment = (NavigatorFragment) getSupportFragmentManager().findFragmentById(R.id.main_navigator);
        navigatorFragment.InitDrawToggle(mDrawerLayout);

        //��ȡ��Ļ��Ϣ
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screenWidth = ZDisplay.Px2Dp(getMyApplication(), dm.widthPixels);
        getMyApplication().setScreenWidthInDP(screenWidth);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mIsNightMode != getMyApplication().isNightMode()) {
            recreateOnResume();
        }
    }

    @Override
    public void OpenNavigator() {
        mDrawerLayout.openDrawer(mNavigatorView);
    }

    @Override
    public void CloseNavigator() {
        mDrawerLayout.closeDrawer(mNavigatorView);
    }

    @Override
    public void OnClickNews() {
        ReplaceFragment(NewsListFragment.newInstance());
    }

    @Override
    public void OnClickOpenSource() {
        ReplaceFragment(NewsListFragment.newInstance());
    }

    public void ReplaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.main_content, fragment)
                .commit();
    }
}
