package zhexian.app.zoschina.main;

import java.util.ArrayList;
import java.util.List;

/**
 * 导航数据操作类
 * Created by Administrator on 2015/5/5.
 */
public class NavigatorDal {
    public static final int ID_NEWS = 1;
    public static final int ID_OPEN_SOURCE = 2;

    private static NavigatorDal mDal;

    public static NavigatorDal getInstance() {
        if (mDal == null)
            mDal = new NavigatorDal();

        return mDal;
    }

    public String getNavigatorName(int id) {
        switch (id) {
            case ID_NEWS:
                return "精选资讯";
            case ID_OPEN_SOURCE:
                return "开源软件";
            default:
                return "";
        }
    }

    public List<NavigatorModel> getList() {
        List<NavigatorModel> list = new ArrayList<>();

        list.add(new NavigatorModel(ID_NEWS, getNavigatorName(ID_NEWS), false));
        list.add(new NavigatorModel(ID_OPEN_SOURCE, getNavigatorName(ID_OPEN_SOURCE), false));

        return list;
    }
}
