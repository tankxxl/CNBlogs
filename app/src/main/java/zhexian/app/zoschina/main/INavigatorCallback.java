package zhexian.app.zoschina.main;

/**
 * Created by Administrator on 2015/5/4.
 */
public interface INavigatorCallback {
    void OpenNavigator();

    void CloseNavigator();

    void OnClickNews();

    void OnClickOpenSource();
}
