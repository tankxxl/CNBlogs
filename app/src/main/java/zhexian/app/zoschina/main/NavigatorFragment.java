package zhexian.app.zoschina.main;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import zhexian.app.zoschina.R;
import zhexian.app.zoschina.base.BaseActionBarActivity;
import zhexian.app.zoschina.favorite.FavoriteActivity;


/**
 * 导航fragment
 */
public class NavigatorFragment extends Fragment implements View.OnClickListener {

    /**
     * 用户展开过侧边栏吗
     */
    private static final String PARAM_IS_USER_LEARNED_NAVIGATOR = "PARAM_IS_USER_LEARNED_NAVIGATOR";


    /**
     * 侧边栏当前选中的位置
     */
    private static final String PARAM_NAVIGATOR_SELECTED_POSITION = "PARAM_NAVIGATOR_SELECTED_POSITION";

    private BaseActionBarActivity mBaseActivity;
    private View mSettingBtn;
    private View mFavoriteBtn;
    private ListView mNavigatorItemList;
    private NavigatorArrayAdapter mNavigatorAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private INavigatorCallback mINavigatorCallBack;

    private int mCurrentSelectedPosition = 0;
    private boolean mIsUserLearnedNavigator = false;
    private boolean mIsFromSavedInstance = false;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mBaseActivity = (BaseActionBarActivity) activity;
        mINavigatorCallBack = (INavigatorCallback) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsUserLearnedNavigator = PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(PARAM_IS_USER_LEARNED_NAVIGATOR, false);

        if (savedInstanceState != null) {
            mIsFromSavedInstance = true;
            mCurrentSelectedPosition = savedInstanceState.getInt(PARAM_NAVIGATOR_SELECTED_POSITION);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_navigator, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mNavigatorItemList = (ListView) view.findViewById(R.id.main_navigatorList);
        mNavigatorAdapter = new NavigatorArrayAdapter(getActivity(), R.layout.main_side_navigator_item, NavigatorDal.getInstance().getList());
        mNavigatorItemList.setAdapter(mNavigatorAdapter);

        mNavigatorItemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onSelectNavigator(position);
            }
        });

        mNavigatorItemList.setItemChecked(mCurrentSelectedPosition, true);
        mSettingBtn = view.findViewById(R.id.navigator_setting);
        mFavoriteBtn = view.findViewById(R.id.navigator_my_favorite);

        mSettingBtn.setOnClickListener(this);
        mFavoriteBtn.setOnClickListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PARAM_NAVIGATOR_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mINavigatorCallBack = null;
    }

    void onSelectNavigator(int position) {
        mCurrentSelectedPosition = position;
        mNavigatorAdapter.notifyDataSetChanged();

        if (mNavigatorItemList != null)
            mNavigatorItemList.setItemChecked(position, true);

        if (mINavigatorCallBack == null)
            return;

        mINavigatorCallBack.CloseNavigator();
        int navigatorID = (int) mNavigatorAdapter.getItemId(position);

        switch (navigatorID) {
            case NavigatorDal.ID_NEWS:
                mINavigatorCallBack.OnClickNews();
                break;
            case NavigatorDal.ID_OPEN_SOURCE:
                mINavigatorCallBack.OnClickOpenSource();
                break;
        }
    }

    void InitDrawToggle(DrawerLayout drawerLayout) {
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, R.drawable.ic_drawer, R.string.open_navigator, R.string.close_navigator) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);


                if (mIsUserLearnedNavigator)
                    return;

                mIsUserLearnedNavigator = true;
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
                sp.edit().putBoolean(PARAM_IS_USER_LEARNED_NAVIGATOR, true).apply();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        if (!mIsUserLearnedNavigator && !mIsFromSavedInstance)
            mINavigatorCallBack.OpenNavigator();

        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        drawerLayout.setDrawerListener(mDrawerToggle);
        onSelectNavigator(mCurrentSelectedPosition);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.navigator_setting: {
                mINavigatorCallBack.CloseNavigator();
                SettingActivity.actionStart(mBaseActivity);
            }
            break;

            case R.id.navigator_my_favorite:
                mINavigatorCallBack.CloseNavigator();
                FavoriteActivity.actionStart(mBaseActivity);
                break;
        }
    }
}
