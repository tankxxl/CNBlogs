package zhexian.app.zoschina.main;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import zhexian.app.zoschina.R;
import zhexian.app.zoschina.base.BaseActionBarActivity;
import zhexian.app.zoschina.base.BaseApplication;
import zhexian.app.zoschina.lib.ZIO;
import zhexian.app.zoschina.util.Utils;

public class SettingActivity extends BaseActionBarActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private CheckBox mCKBIsAutoLoadNews;
    private CheckBox mCKBImageOnlyWifi;
    private CheckBox mCKBIsBigFont;
    private CheckBox mCKBIsNightMode;
    private View mViewCleanCache;
    private TextView mTVCacheSize;
    private BaseApplication baseApp;
    private View mCleanCacheProgress;
    private boolean mIsDoTask = false;
    private String emptyStr;
    private boolean mIsCleanAll = false;

    public static void actionStart(Context context) {
        Intent intent = new Intent(context, SettingActivity.class);
        context.startActivity(intent);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        emptyStr = getResources().getString(R.string.dir_empty);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        baseApp = getMyApplication();

        mCKBIsAutoLoadNews = (CheckBox) findViewById(R.id.setting_is_auto_load_news);
        mCKBIsAutoLoadNews.setChecked(baseApp.isAutoLoadRecommend());
        mCKBIsAutoLoadNews.setOnCheckedChangeListener(this);

        mCKBImageOnlyWifi = (CheckBox) findViewById(R.id.setting_is_image_only_wifi);
        mCKBImageOnlyWifi.setChecked(baseApp.isImgOnlyWifi());
        mCKBImageOnlyWifi.setOnCheckedChangeListener(this);

        mCKBIsBigFont = (CheckBox) findViewById(R.id.setting_is_big_font);
        mCKBIsBigFont.setChecked(baseApp.isBigFont());
        mCKBIsBigFont.setOnCheckedChangeListener(this);

        mCKBIsNightMode = (CheckBox) findViewById(R.id.setting_is_night_mode);
        mCKBIsNightMode.setChecked(baseApp.isNightMode());
        mCKBIsNightMode.setOnCheckedChangeListener(this);

        mViewCleanCache = findViewById(R.id.setting_clean_cache);
        mViewCleanCache.setOnClickListener(this);

        mTVCacheSize = (TextView) findViewById(R.id.setting_cache_size);
        String sizeText = ZIO.getDirSizeInfo(baseApp.getCachePath(), emptyStr);
        mTVCacheSize.setText(sizeText);

        mIsCleanAll = sizeText.equals(emptyStr);

        mCleanCacheProgress = findViewById(R.id.setting_cache_progress);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return true;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        int id = compoundButton.getId();
        switch (id) {
            case R.id.setting_is_auto_load_news:
                baseApp.setIsAutoLoadRecommend(b);
                break;
            case R.id.setting_is_image_only_wifi:
                baseApp.setIsImgOnlyWifi(b);
                break;
            case R.id.setting_is_big_font:
                baseApp.setIsBigFont(b);
                break;
            case R.id.setting_is_night_mode:
                changeViewMode();
                break;
        }
    }

    void changeViewMode() {
        boolean isNight = getMyApplication().isNightMode();
        if (isNight)
            ChangeToDay();
        else
            ChangeToNight();

        recreate();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.setting_clean_cache:
                onCleanCacheClick();
                break;
        }
    }

    void onCleanCacheClick() {

        if (mIsCleanAll) {
            Utils.toast(this, "已经清理干净。");
            return;
        }

        if (mIsDoTask)
            return;

        new CleanCacheTask().execute(baseApp.getCachePath());
    }

    class CleanCacheTask extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            mIsDoTask = true;
            mCleanCacheProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(String... params) {
            ZIO.emptyDir(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mIsDoTask = false;
            mIsCleanAll = true;
            mCleanCacheProgress.setVisibility(View.GONE);
            mTVCacheSize.setText(emptyStr);
            Utils.toast(baseApp, "缓存清理完成");
        }
    }
}
