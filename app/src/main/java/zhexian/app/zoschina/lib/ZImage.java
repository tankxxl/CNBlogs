package zhexian.app.zoschina.lib;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;
import android.widget.ImageView;

import java.io.FileOutputStream;
import java.io.IOException;

import zhexian.app.zoschina.R;
import zhexian.app.zoschina.base.BaseActionBarActivity;
import zhexian.app.zoschina.base.BaseApplication;
import zhexian.app.zoschina.util.Utils;

public class ZImage {

    /**
     * 超过就不走内存缓存了。可以走磁盘缓存
     */
    private static final int MAX_CACHED_IMAGE_SIZE = 200 * 1024;


    private static final int CACHED_MEMORY_SIZE = 10 * 1024 * 1024;

    private static ZImage mZImage;
    LruCache<String, Bitmap> mMemoryCache;

    private BaseApplication mApp;
    private Point screenSize;
    private Bitmap placeHolderBitmap;

    public ZImage(BaseActionBarActivity activity) {
        mApp = activity.getMyApplication();
        screenSize = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(screenSize);
        placeHolderBitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.image_place_holder);

        mMemoryCache = new LruCache<String, Bitmap>(CACHED_MEMORY_SIZE) {
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount();
            }
        };
    }

    public static void Init(BaseActionBarActivity activity) {
        if (mZImage == null)

            synchronized (ZImage.class) {
                if (mZImage == null)
                    mZImage = new ZImage(activity);
            }
    }

    public static ZImage getInstance() {
        if (mZImage == null) {
            Log.d("图片模块未初始化", "调用Init来初始化");
        }
        return mZImage;
    }

    public void loadEmpty(ImageView imageView) {
        imageView.setImageBitmap(placeHolderBitmap);
    }

    /**
     * 加载图片到内存中
     *
     * @param url          地址
     * @param imageView    图片
     * @param width        宽度
     * @param height       高度
     * @param isCache      是否缓存到磁盘与内存中
     * @param canQueryHttp 如果找不到本地文件，是否可以通过网络获取
     */
    public void load(String url, ImageView imageView, int width, int height, boolean isCache, boolean canQueryHttp) {
        url = ZString.toBrowserCode(url);
        imageView.setTag(url);

        //L1 内存
        Bitmap bitmap = getFromMemoryCache(url);

        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
            return;
        }

        //L2 磁盘
        String cachedUrl = ZString.getFileCachedDir(url, mApp.getCachePath());

        if (ZIO.isExist(cachedUrl)) {
            bitmap = Utils.getScaledBitMap(cachedUrl, width, height);

            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);

                if (isCache)
                    putToMemoryCache(url, bitmap);

                return;
            }
        }
        //L3 网络
        if (canQueryHttp) {
            new ImageLoadTask().execute(new ImageEntity(url, imageView, width, height, isCache));
        } else
            loadEmpty(imageView);
    }

    public String saveToCache(String url) {
        return saveToCache(url, screenSize.x, screenSize.y);
    }

    public String saveToCache(String url, int width, int height) {
        url = ZString.toBrowserCode(url);
        String cachedUrl = ZString.getFileCachedDir(url, mApp.getCachePath());

        if (ZIO.isExist(cachedUrl) == false)
            saveBitmapToCache(ZHttp.getBitmap(url, width, height), cachedUrl);

        return cachedUrl;
    }

    public void saveToCache(Bitmap bitmap, String url) {
        String cachedUrl = ZString.getFileCachedDir(url, mApp.getCachePath());

        if (ZIO.isExist(cachedUrl))
            return;

        saveBitmapToCache(bitmap, cachedUrl);
    }

    private void saveBitmapToCache(Bitmap bitmap, String cachedUrl) {
        ZIO.createFile(cachedUrl);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(cachedUrl);
            Bitmap.CompressFormat format = cachedUrl.toLowerCase().indexOf("jpeg") > 0 ? Bitmap.CompressFormat.JPEG : Bitmap.CompressFormat.PNG;

            if (fos != null && bitmap != null && bitmap.getByteCount() > 0) {
                bitmap.compress(format, 100, fos);
                fos.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    Bitmap getFromMemoryCache(String url) {
        return mMemoryCache.get(url);
    }

    void putToMemoryCache(String url, Bitmap bitmap) {
        if (bitmap != null && bitmap.getByteCount() < MAX_CACHED_IMAGE_SIZE)
            mMemoryCache.put(url, bitmap);
    }

    class ImageEntity {
        String url;
        ImageView imageView;
        int width;
        int height;
        boolean isCache;

        public ImageEntity(String url, ImageView imageView, int width, int height, boolean isCache) {
            this.url = url;
            this.imageView = imageView;
            this.width = width;
            this.height = height;
            this.isCache = isCache;
        }
    }

    class ImageLoadTask extends AsyncTask<ImageEntity, Void, Bitmap> {
        ImageEntity imageEntity;

        @Override
        protected Bitmap doInBackground(ImageEntity... imageEntities) {
            imageEntity = imageEntities[0];

            return ZHttp.getBitmap(imageEntity.url, imageEntity.width, imageEntity.height);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            if (imageEntity.imageView.getTag().equals(imageEntity.url))
                imageEntity.imageView.setImageBitmap(bitmap);

            if (imageEntity.isCache) {
                saveToCache(bitmap, imageEntity.url);

                if (getFromMemoryCache(imageEntity.url) == null)
                    putToMemoryCache(imageEntity.url, bitmap);
            }
        }
    }
}
