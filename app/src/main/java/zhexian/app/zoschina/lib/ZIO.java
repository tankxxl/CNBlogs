package zhexian.app.zoschina.lib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.Date;

/**
 * 文件操作类
 */
public class ZIO {

    /**
     * 清空文件夹
     *
     * @param dir
     * @return
     */
    public static boolean emptyDir(File dir) {
        if (dir.isDirectory()) {
            for (File f : dir.listFiles())
                emptyDir(f);
        }
        return dir.delete();
    }

    public static boolean emptyDir(String url) {
        File file = new File(url);
        if (file.exists())
            return emptyDir(file);

        return true;
    }

    /**
     * 如果目录不存在，则创建
     *
     * @param url
     */
    public static void mkDirs(String url) {
        File file = new File(url);
        if (!file.exists())
            file.mkdirs();
    }

    /**
     * 文件是否存在
     *
     * @param url
     * @return
     */
    public static boolean isExist(String url) {
        File file = new File(url);
        return file.exists();
    }

    public static boolean deleteFile(String url) {
        File file = new File(url);
        return file.delete();
    }

    public static boolean createFile(String url) {
        File file = new File(url);

        if (file.exists())
            return true;

        String dir = url.substring(0, url.lastIndexOf('/'));
        mkDirs(dir);

        try {
            file.createNewFile();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * 将对象序列化到本地文件中
     *
     * @param obj
     * @param url
     * @return
     */
    public static boolean writeToFile(Object obj, String url) {
        FileOutputStream fileStream = null;
        ObjectOutputStream objectStream = null;

        try {
            File file = new File(url);

            if (file.exists() == false)
                file.createNewFile();

            fileStream = new FileOutputStream(file);
            objectStream = new ObjectOutputStream(fileStream);
            objectStream.writeObject(obj);
            objectStream.flush();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (fileStream != null)
                try {
                    fileStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            if (objectStream != null)
                try {
                    objectStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
        }
    }

    /**
     * 从文件中反序列化类型
     *
     * @param url
     * @return
     */
    public static Object readFromFile(String url) {
        if (isExist(url) == false)
            return null;

        Object object = null;
        FileInputStream fileStream = null;
        ObjectInputStream objectStream = null;

        try {
            fileStream = new FileInputStream(url);
            objectStream = new ObjectInputStream(fileStream);
            object = objectStream.readObject();
            return object;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fileStream != null)
                try {
                    fileStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            if (objectStream != null)
                try {
                    objectStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
        }
        //执行到这里已经反序列化失败了，此时把无效的文件删除
        deleteFile(url);
        return object;
    }

    /**
     * 缓存文件是否可用，1：存在 2：修改时间比过期时间新
     *
     * @param url              文件地址
     * @param diffMilliSeconds 缓存有效时间，单位毫秒
     * @return
     */
    public static boolean IsCachedFileAvailable(String url, int diffMilliSeconds) {
        File file = new File(url);

        if (file.exists() == false)
            return false;

        long earlyTime = new Date().getTime() - diffMilliSeconds;
        return file.lastModified() > earlyTime;
    }

    public static String readString(InputStream in) {
        String content;
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        try {
            while ((content = reader.readLine()) != null) {
                sb.append(content);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb
                .toString();
    }

    public static String getDirSizeInfo(String url, String emptyDescription) {

        File file = new File(url);
        if (file.exists() == false)
            return emptyDescription;

        long size = getDirCapacity(file);

        if (size == 0)
            return emptyDescription;

        return formatFileSize(size);
    }

    /**
     * 转换文件大小
     *
     * @param fileS
     * @return B/KB/MB/GB
     */
    public static String formatFileSize(long fileS) {
        java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
        String fileSizeString;
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format((double) fileS / 1024) + "KB";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format((double) fileS / 1048576) + "MB";
        } else {
            fileSizeString = df.format((double) fileS / 1073741824) + "G";
        }
        return fileSizeString;
    }

    /**
     * 获取目录文件大小
     *
     * @param dir
     * @return
     */
    public static long getDirCapacity(File dir) {
        if (dir == null) {
            return 0;
        }
        if (!dir.isDirectory()) {
            return 0;
        }
        long dirSize = 0;
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                dirSize += file.length();
            } else if (file.isDirectory()) {
                dirSize += file.length();
                dirSize += getDirCapacity(file); // 递归调用继续统计
            }
        }
        return dirSize;
    }

    /**
     * 获取目录文件个数
     *
     * @param
     * @return
     */
    public static long getDirCount(File dir) {
        long count;
        File[] files = dir.listFiles();
        count = files.length;
        for (File file : files) {
            if (file.isDirectory()) {
                count = count + getDirCount(file);// 递归
                count--;
            }
        }
        return count;
    }
}
