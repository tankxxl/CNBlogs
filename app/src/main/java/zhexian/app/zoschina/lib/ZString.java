package zhexian.app.zoschina.lib;

/**
 * Created by kimmy on 2015/5/16.
 */
public class ZString {
    private static final String HEX_STRING = "0123456789ABCDEF";

    /**
     * 获取第一个分割符的位置 跳过https://这八个字符
     */
    private static final int HTTP_FIRST_SPLIT_POS = 8;

    /**
     * 把中文字符转换为带百分号的浏览器编码
     *
     * @param word
     * @return
     */
    public static String toBrowserCode(String word) {
        byte[] bytes = word.getBytes();

        //不包含中文，不做处理
        if (bytes.length == word.length())
            return word;

        StringBuilder browserUrl = new StringBuilder();
        String tempStr = "";

        for (int i = 0; i < word.length(); i++) {
            char currentChar = word.charAt(i);

            //不需要处理
            if ((int) currentChar <= 256) {

                if (tempStr.length() > 0) {
                    byte[] cBytes = tempStr.getBytes();

                    for (int j = 0; j < cBytes.length; j++) {
                        browserUrl.append('%');
                        browserUrl.append(HEX_STRING.charAt((cBytes[j] & 0xf0) >> 4));
                        browserUrl.append(HEX_STRING.charAt((cBytes[j] & 0x0f) >> 0));
                    }
                    tempStr = "";
                }

                browserUrl.append(currentChar);
            } else {
                //把要处理的字符，添加到队列中
                tempStr += currentChar;
            }
        }
        return browserUrl.toString();
    }

    /**
     * 将url转换为本地缓存地址
     *
     * @param url       文件地址
     * @param cachedDir 缓存目录
     * @return 本地路径
     */
    public static String getFileCachedDir(String url, String cachedDir) {
        //将地址 http://images.cnitblog.com/news_topic/apple.png 截取成 news_topic/apple.png
        url = url.substring(url.indexOf('/', HTTP_FIRST_SPLIT_POS) + 1);
        url = cachedDir + url;

        return url;
    }

}
