package zhexian.app.zoschina.lib;

import android.graphics.Bitmap;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import zhexian.app.zoschina.util.Utils;

/**
 * Created by kimmy on 2015/5/16.
 */
public class ZHttp {
    private static OkHttpClient mOkHttpClient;

    public static OkHttpClient getHttpClient() {

        if (mOkHttpClient == null) {
            synchronized (Utils.class) {
                mOkHttpClient = new OkHttpClient();
                mOkHttpClient.setConnectTimeout(6, TimeUnit.SECONDS);
                mOkHttpClient.setReadTimeout(6, TimeUnit.SECONDS);
            }
        }
        return mOkHttpClient;
    }

    public static Response execute(String url) {
        Request request = new Builder().url(url).build();
        try {
            Response response = getHttpClient().newCall(request).execute();

            if (response.isSuccessful())
                return response;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 通过get请求，获取json实例
     *
     * @param urlStr 请求地址
     */
    public static String getString(String urlStr) {
        Response response = execute(urlStr);

        if (response == null)
            return null;

        try {
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取网落图片资源
     *
     * @param url
     * @return
     */
    public static Bitmap getBitmap(String url, int width, int height) {
        Bitmap bitmap = null;

        try {
            Response response = execute(url);
            if (null == response)
                return null;

            bitmap = Utils.getScaledBitMap(response.body().bytes(), width, height);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
