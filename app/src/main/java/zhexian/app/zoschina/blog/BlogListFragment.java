package zhexian.app.zoschina.blog;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import zhexian.app.zoschina.CustomControl.ITabActionCallback;
import zhexian.app.zoschina.CustomControl.TabActionBarView;
import zhexian.app.zoschina.R;
import zhexian.app.zoschina.base.BaseActionBarActivity;
import zhexian.app.zoschina.base.BaseSwipeListFragment;
import zhexian.app.zoschina.news.NewsListEntity;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlogListFragment extends BaseSwipeListFragment<NewsListEntity> implements ITabActionCallback {

    public static BlogListFragment newInstance() {
        return new BlogListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blog_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TabActionBarView actionBarView = new TabActionBarView((BaseActionBarActivity) getActivity(), this);
        actionBarView.bindTab("精选", "热门", "最新");
    }

    @Override
    protected RecyclerView.Adapter<RecyclerView.ViewHolder> bindArrayAdapter(List<NewsListEntity> list) {
        return null;
    }

    @Override
    protected List<NewsListEntity> loadData(int pageIndex, int pageSize) {
        return null;
    }


    @Override
    public void onFirstTabClick() {

    }

    @Override
    public void onSecondTabClick() {

    }

    @Override
    public void onThirdClick() {

    }
}
