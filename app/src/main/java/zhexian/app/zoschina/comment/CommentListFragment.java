package zhexian.app.zoschina.comment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import zhexian.app.zoschina.base.BaseSwipeListFragment;
import zhexian.app.zoschina.util.ConfigConstant;

/**
 * Created by Administrator on 2015/6/6.
 */
public class CommentListFragment extends BaseSwipeListFragment<CommentEntity> {
    private ConfigConstant.CommentCategory mCategory;
    private long mDataID;

    public static CommentListFragment fragmentStart(ConfigConstant.CommentCategory category, long dataID) {
        CommentListFragment fragment = new CommentListFragment();
        Bundle args = new Bundle();
        args.putSerializable(CommentActivity.PARAM_CATEGORY, category);
        args.putLong(CommentActivity.PARAM_DATA_ID, dataID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCategory = (ConfigConstant.CommentCategory) getArguments().getSerializable(CommentActivity.PARAM_CATEGORY);
        mDataID = getArguments().getLong(CommentActivity.PARAM_DATA_ID);
        showLoadingIndicatorTask();
        onRefresh();
    }

    @Override
    protected RecyclerView.Adapter<RecyclerView.ViewHolder> bindArrayAdapter(List<CommentEntity> list) {
        return new CommentAdapter(mBaseActionBarActivity, list);
    }

    @Override
    protected List<CommentEntity> loadData(int pageIndex, int pageSize) {
        return CommentDal.getCommentList(mBaseApplication, mCategory, mDataID, pageIndex, pageSize);
    }
}
