package zhexian.app.zoschina.comment;

import java.util.List;

import zhexian.app.zoschina.base.BaseApplication;
import zhexian.app.zoschina.lib.ZHttp;
import zhexian.app.zoschina.util.ConfigConstant;

/**
 * Created by Administrator on 2015/6/6.
 */
public class CommentDal {

    private static String generateKey(ConfigConstant.CommentCategory category, long dataID, int pageIndex, int pageSize) {
        String prefix;
        if (category == ConfigConstant.CommentCategory.News)
            prefix = "news";
        else
            prefix = "blog";

        return String.format("%s_comment_%d_%d_%d", prefix, dataID, pageIndex, pageSize);
    }

    private static String generateUrl(ConfigConstant.CommentCategory category, long dataID, int pageIndex, int pageSize) {
        if (category == ConfigConstant.CommentCategory.News)
            return String.format("http://wcf.open.cnblogs.com/news/item/%d/comments/%d/%d", dataID, pageIndex, pageSize);
        else
            return String.format("http://wcf.open.cnblogs.com/blog/post/%d/comments/%d/%d", dataID, pageIndex, pageSize);
    }

    public static List<CommentEntity> getCommentList(BaseApplication baseApp, ConfigConstant.CommentCategory category, long dataID, int pageIndex, int pageSize) {

        List<CommentEntity> listEntity;
        String key = generateKey(category, dataID, pageIndex, pageSize);

        if (baseApp.isNetworkAvailable() == false && baseApp.isFileCached(key)) {
            listEntity = (List<CommentEntity>) baseApp.ReadFromCache(key);
        } else {
            String xmlStr = generateUrl(category, dataID, pageIndex, pageSize);
            listEntity = CommentEntity.ParseXML(ZHttp.getString(xmlStr));
            baseApp.SaveToCache(listEntity, key);
        }
        return listEntity;
    }
}
