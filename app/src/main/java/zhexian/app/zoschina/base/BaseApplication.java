package zhexian.app.zoschina.base;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

import java.io.IOException;

import zhexian.app.zoschina.lib.ZDate;
import zhexian.app.zoschina.lib.ZIO;
import zhexian.app.zoschina.util.ConfigConstant;
import zhexian.app.zoschina.util.Utils;

/**
 * Created by Administrator on 2015/5/6.
 */
public class BaseApplication extends Application {
    private static final String PARAM_IS_AUTO_LOAD_RECOMMEND = "PARAM_IS_AUTO_LOAD_RECOMMEND";
    private static final String PARAM_IS_NIGHT_MODE = "PARAM_IS_NIGHT_MODE";
    private static final String PARAM_IS_IMG_ONLY_WIFI = "PARAM_IS_IMG_ONLY_WIFI";
    private static final String PARAM_PAGE_SIZE = "PARAM_PAGE_SIZE";
    private static final String PARAM_IS_BIG_FONT = "PARAM_IS_BIG_FONT";
    private SharedPreferences mSp;
    //是否夜间模式
    private boolean mIsNightMode;

    //是否仅在wifi下下载图片
    private boolean mIsImgOnlyWifi;
    //是否自动加载新闻
    private boolean mIsAutoLoadRecommend;

    private boolean mIsBigFont;
    //每次请求页面大小
    private int mPageSize;

    private String mCachePath;
    private ConfigConstant.NetworkStatus mNetWorkStatus;

    private String mHtmlString;

    private int screenWidthInDP;

    @Override
    public void onCreate() {
        super.onCreate();
        mSp = PreferenceManager.getDefaultSharedPreferences(this);
        mIsAutoLoadRecommend = mSp.getBoolean(PARAM_IS_AUTO_LOAD_RECOMMEND, true);
        mIsNightMode = mSp.getBoolean(PARAM_IS_NIGHT_MODE, false);
        mPageSize = mSp.getInt(PARAM_PAGE_SIZE, 15);
        mIsImgOnlyWifi = mSp.getBoolean(PARAM_IS_IMG_ONLY_WIFI, false);
        mIsBigFont = mSp.getBoolean(PARAM_IS_BIG_FONT, false);
        mCachePath = Environment.isExternalStorageEmulated() ? getExternalFilesDir(null).getAbsolutePath() : getFilesDir().getAbsolutePath();
        mCachePath += "/";
        mNetWorkStatus = Utils.GetConnectType(this);

        try {
            mHtmlString = ZIO.readString(getAssets().open("content.html"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isAutoLoadRecommend() {
        return mIsAutoLoadRecommend;
    }

    public void setIsAutoLoadRecommend(boolean isAutoLoadRecommend) {
        if (mIsAutoLoadRecommend == isAutoLoadRecommend)
            return;

        mIsAutoLoadRecommend = isAutoLoadRecommend;
        mSp.edit().putBoolean(PARAM_IS_AUTO_LOAD_RECOMMEND, mIsAutoLoadRecommend).apply();
    }

    public boolean isBigFont() {
        return mIsBigFont;
    }

    public void setIsBigFont(boolean isBigFont) {
        if (mIsBigFont == isBigFont)
            return;

        mIsBigFont = isBigFont;
        mSp.edit().putBoolean(PARAM_IS_BIG_FONT, mIsBigFont).apply();
    }

    public boolean isNightMode() {
        return mIsNightMode;
    }

    public void setIsNightMode(boolean isNightMode) {
        if (mIsNightMode == isNightMode)
            return;

        mIsNightMode = isNightMode;
        mSp.edit().putBoolean(PARAM_IS_NIGHT_MODE, mIsNightMode).apply();
    }

    public int getPageSize() {
        return mPageSize;
    }

    public void setPageSize(int pageSize) {
        if (mPageSize == pageSize)
            return;

        mPageSize = pageSize;
        mSp.edit().putInt(PARAM_PAGE_SIZE, mPageSize).apply();
    }

    public boolean isImgOnlyWifi() {
        return mIsImgOnlyWifi;
    }

    public void setIsImgOnlyWifi(boolean isImgOnlyWifi) {
        if (mIsImgOnlyWifi == isImgOnlyWifi)
            return;

        mIsImgOnlyWifi = isImgOnlyWifi;
        mSp.edit().putBoolean(PARAM_IS_IMG_ONLY_WIFI, mIsImgOnlyWifi).apply();
    }

    public boolean SaveToCache(Object obj, String key) {
        ZIO.mkDirs(mCachePath);
        return ZIO.writeToFile(obj, mCachePath + key);
    }

    public Object ReadFromCache(String key) {
        return ZIO.readFromFile(mCachePath + key);
    }

    /**
     * 缓存存在，并且在有效时间内
     *
     * @param key 缓存的key
     * @return
     */
    public boolean IsCacheFileAvailable(String key) {
        return ZIO.IsCachedFileAvailable(mCachePath + key, ZDate.getCacheMilliSeconds(mNetWorkStatus));
    }


    /**
     * 文件是否被缓存
     *
     * @param key
     * @return
     */
    public boolean isFileCached(String key) {
        return ZIO.isExist(mCachePath + key);
    }


    public ConfigConstant.NetworkStatus getNetworkStatus() {
        return mNetWorkStatus;
    }

    public void setNetworkStatus(ConfigConstant.NetworkStatus mNetworkStatus) {
        this.mNetWorkStatus = mNetworkStatus;
    }

    public boolean isNetworkAvailable() {
        return mNetWorkStatus != ConfigConstant.NetworkStatus.DisConnect;
    }

    public boolean isNetworkWifi() {
        return mNetWorkStatus == ConfigConstant.NetworkStatus.Wifi;
    }

    /**
     * 是否可以载入图片
     *
     * @return
     */
    public boolean canRequestImage() {
        return mNetWorkStatus == ConfigConstant.NetworkStatus.Wifi ||
                (mNetWorkStatus == ConfigConstant.NetworkStatus.Mobile && !mIsImgOnlyWifi);
    }

    public String getCachePath() {
        return mCachePath;
    }

    public String getHtmlString() {
        return mHtmlString;
    }

    public int getScreenWidthInDP() {
        return screenWidthInDP;
    }

    public void setScreenWidthInDP(int screenWidthInDP) {
        this.screenWidthInDP = screenWidthInDP;
    }
}
