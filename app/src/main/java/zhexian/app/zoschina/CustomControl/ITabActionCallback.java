package zhexian.app.zoschina.CustomControl;

/**
 * Created by Administrator on 2015/6/4.
 */
public interface ITabActionCallback {
    void onFirstTabClick();

    void onSecondTabClick();

    void onThirdClick();
}
