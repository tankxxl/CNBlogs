package zhexian.app.zoschina.util;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.webkit.JavascriptInterface;

import zhexian.app.zoschina.lib.ZImage;

/**
 * Created by kimmy on 2015/6/1.
 */
public class WebViewJsInterface {

    Activity mActivity;

    public WebViewJsInterface(Activity activity) {
        mActivity = activity;
    }

    @JavascriptInterface
    public void displayImage(String url) {
        new DisplayTask().execute(url);
    }

    class DisplayTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            return ZImage.getInstance().saveToCache(strings[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            if (mActivity.isFinishing())
                return;

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(Utils.URIToLocal(s)), "image/*");
            mActivity.startActivity(intent);
        }
    }
}
