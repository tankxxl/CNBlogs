package zhexian.app.zoschina.util;

import java.util.Iterator;
import java.util.List;

/**
 * Created by kimmy on 2015/6/4.
 */
public class Sys {
    public static <T> void SubList(List<T> list, int count) {
        Iterator<T> iterator = list.iterator();

        int index = 0;
        while (iterator.hasNext() && index < count) {
            iterator.next();
            iterator.remove();
            index++;
        }
    }
}
