package zhexian.app.zoschina.util;

import android.animation.AnimatorInflater;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import zhexian.app.zoschina.base.BaseApplication;

/**
 * 工具类
 */
public class Utils {

    /**
     * 创建一个颜色渐变的动画类
     *
     * @param context    上下文
     * @param animatorID 动画资源ID
     * @param target     影响对象
     * @return
     */
    public static ObjectAnimator GenerateColorAnimator(Context context, int animatorID, Object target) {
        ObjectAnimator colorAnimation = (ObjectAnimator) AnimatorInflater.loadAnimator(context, animatorID);
        colorAnimation.setTarget(target);
        colorAnimation.setEvaluator(new ArgbEvaluator());
        return colorAnimation;
    }


    public static void toast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static Bitmap getScaledBitMap(String imgPath, int width, int height) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imgPath, options);

        float srcWidth = options.outWidth;
        float srcHeight = options.outHeight;
        int inSampleSize = 1;

        if (srcHeight > height || srcWidth > width) {
            if (srcWidth > srcHeight)
                inSampleSize = Math.round(srcHeight / height);
            else
                inSampleSize = Math.round(srcWidth / width);
        }

        options = new BitmapFactory.Options();
        options.inSampleSize = inSampleSize;

        Bitmap bitmap = BitmapFactory.decodeFile(imgPath, options);
        return bitmap;
    }

    public static Bitmap getScaledBitMap(byte[] data, int width, int height) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, 0, data.length, options);

        float srcWidth = options.outWidth;
        float srcHeight = options.outHeight;
        int inSampleSize = 1;

        if (srcHeight > height || srcWidth > width) {
            if (srcWidth > srcHeight)
                inSampleSize = Math.round(srcHeight / height);
            else
                inSampleSize = Math.round(srcWidth / width);
        }

        options = new BitmapFactory.Options();
        options.inSampleSize = inSampleSize;

        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);

        return bitmap;
    }

    public static String decorateIMGTag(String htmlContent, int screenWidth, boolean canRequest) {
        Pattern patternImgSrc = Pattern.compile("<img(.+?)src=\"(.+?)\"(.+?)/>");
        Matcher localMatcher = patternImgSrc.matcher(htmlContent);
        //-16个dp,是为了让图片两侧留有空隙，符合安卓设计风格
        screenWidth -= 16;

        while (true) {
            if (!(localMatcher.find()))
                return htmlContent;

            String jsStr = "onclick=\"showImage(this,'$2')\"";
            String src;

            if (canRequest)
                src = "$2";
            else
                src = "click_load.png";

            htmlContent = localMatcher.replaceAll(String.format("<img src='%s' %s style='max-width:%dpx'/>", src, jsStr, screenWidth));
        }
    }

    /**
     * 获取网络类型
     *
     * @param context
     * @return
     */
    public static ConfigConstant.NetworkStatus GetConnectType(Context context) {
        ConnectivityManager connMgr =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeInfo = connMgr.getActiveNetworkInfo();

        if (activeInfo != null && activeInfo.isConnected()) {
            if (activeInfo.getType() == ConnectivityManager.TYPE_WIFI)
                return ConfigConstant.NetworkStatus.Wifi;
            else if (activeInfo.getType() == ConnectivityManager.TYPE_MOBILE)
                return ConfigConstant.NetworkStatus.Mobile;
        }

        return ConfigConstant.NetworkStatus.DisConnect;
    }

    public static String URIToLocal(String url) {
        return String.format("file://%s", url);
    }

    public static String getHTMLCSS(BaseApplication baseApplication) {
        return baseApplication.isNightMode() ? "style_night.css" : "style_day.css";
    }
}