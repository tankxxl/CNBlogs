package zhexian.app.zoschina.util;


/**
 * 一些配置常量
 */
public class ConfigConstant {

    public static final double HTML_FONT_SIZE_NORMAL = 1.2;
    public static final double HTML_FONT_SIZE_BIG = 1.4;

    //actionbar显示/隐藏，最小触发时间间隔
    public static final long MIN_CHANGE_DURATION_MILLION_SECONDS = 500;

    //actionbar显示/隐藏，最小触发距离间隔
    public static final long MIN_TRIGGER_ACTION_BAR_DISTANCE = 10;

    /**
     * 列表图片的尺寸
     */
    public static final int LIST_ITEM_IMAGE_SIZE_DP = 90;

    /**
     * 网络状态
     */
    public enum NetworkStatus {
        //没有连接
        DisConnect,

        //流量
        Mobile,

        //Wifi
        Wifi
    }

    /**
     * 信息的分类
     */
    public enum InfoCategory {
        /**
         * 推荐
         */
        Recommend,


        /**
         * 最新
         */
        Recent
    }

    /**
     * 评论的分类
     */
    public enum CommentCategory {
        /**
         * 新闻
         */
        News,


        /**
         * 博客
         */
        Blog
    }
}
