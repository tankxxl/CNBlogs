package zhexian.app.zoschina.news;


import java.util.List;

import zhexian.app.zoschina.base.BaseApplication;
import zhexian.app.zoschina.lib.ZHttp;
import zhexian.app.zoschina.util.ConfigConstant;
import zhexian.app.zoschina.util.Utils;

/**
 * 新闻数据操作类，pageindex从1开始
 */
public class NewsDal {
    public static final String RECOMMEND_STRING = "recommend";
    public static final String RECENT_STRING = "recent";
    static final String endPoint = "http://wcf.open.cnblogs.com/news";
    static final String recommendNewsUrl = String.format("%s/%s/paged/", endPoint, RECOMMEND_STRING);
    static final String recentNewsUrl = String.format("%s/%s/paged/", endPoint, RECENT_STRING);
    static final String newsDetailUrl = String.format("%s/item/", endPoint, RECENT_STRING);

    /**
     * 获取推荐新闻
     * wifi下自动缓存新闻
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public static List<NewsListEntity> getNewsList(BaseApplication baseApp, ConfigConstant.InfoCategory category, int pageIndex, int pageSize) {
        if (baseApp == null)
            return null;

        List<NewsListEntity> listEntity;
        //默认使用最新
        String prefix = RECENT_STRING;
        String requestUrl = recentNewsUrl;

        if (category == ConfigConstant.InfoCategory.Recommend) {
            prefix = RECOMMEND_STRING;
            requestUrl = recommendNewsUrl;
        }

        String key = String.format("%s_news_%d_%d", prefix, pageIndex, pageSize);

        if (baseApp.isNetworkAvailable() == false && baseApp.isFileCached(key)) {
            listEntity = (List<NewsListEntity>) baseApp.ReadFromCache(key);
        } else {
            String xmlStr = ZHttp.getString(String.format("%s%d/%d", requestUrl, pageIndex, pageSize));
            listEntity = NewsListEntity.ParseXML(xmlStr);
            baseApp.SaveToCache(listEntity, key);
        }
        return listEntity;
    }

    public static NewsDetailEntity getNewsDetail(BaseApplication baseApp, long newsID) {
        NewsDetailEntity entity = null;
        String key = String.format("news_content_%d", newsID);

        if (baseApp.isFileCached(key)) {
            entity = (NewsDetailEntity) baseApp.ReadFromCache(key);
        }

        if (entity == null && baseApp.isNetworkAvailable()) {
            String xmlStr = ZHttp.getString(String.format("%s%d", newsDetailUrl, newsID));
            entity = NewsDetailEntity.ParseXML(xmlStr);
        }
        String htmlContent = Utils.decorateIMGTag(entity.getContent(), baseApp.getScreenWidthInDP(), baseApp.canRequestImage());
        entity.setContent(htmlContent);

        return entity;
    }

    public static void CacheNews(BaseApplication baseApp, long newsID) {
        String key = String.format("news_content_%d", newsID);

        if (baseApp.isFileCached(key))
            return;

        String xmlStr = ZHttp.getString(String.format("%s%d", newsDetailUrl, newsID));
        NewsDetailEntity entity = NewsDetailEntity.ParseXML(xmlStr);

        if (entity == null)
            return;

        baseApp.SaveToCache(entity, key);
    }
}
