package zhexian.app.zoschina.news;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import zhexian.app.zoschina.lib.ZDate;
import zhexian.app.zoschina.lib.ZString;

/**
 * 新闻列表数据类
 */
public class NewsListEntity implements Serializable {
    private long newsID;
    private String title;
    private String iconUrl;
    private String publishDate;
    private int recommendAmount;
    private int commentAmount;

    public static List<NewsListEntity> ParseXML(String xmlStr) {
        XmlPullParser parser;

        try {
            parser = XmlPullParserFactory.newInstance().newPullParser();
            parser.setInput(new StringReader(xmlStr));

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        List<NewsListEntity> newsList = null;
        NewsListEntity newsEntity = null;


        try {
            int type = parser.getEventType();

            while (type != XmlPullParser.END_DOCUMENT) {
                switch (type) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        String name = parser.getName();

                        switch (name) {
                            case "feed":
                                newsList = new ArrayList<>();
                                break;
                            case "entry":
                                newsEntity = new NewsListEntity();
                                break;
                            case "id":
                                if (newsEntity != null)
                                    newsEntity.setNewsID(Long.parseLong(parser.nextText()));
                                break;
                            case "title":
                                if (newsEntity != null)
                                    newsEntity.setTitle(parser.nextText());
                                break;
                            case "topicIcon":
                                if (newsEntity != null)
                                    newsEntity.setIconUrl(ZString.toBrowserCode(parser.nextText()));
                                break;
                            case "published":
                                if (newsEntity != null)
                                    newsEntity.setPublishDate(ZDate.FriendlyDate(parser.nextText()));
                                break;
                            case "diggs":
                                if (newsEntity != null)
                                    newsEntity.setRecommendAmount(Integer.parseInt(parser.nextText()));
                                break;
                            case "comments":
                                if (newsEntity != null)
                                    newsEntity.setCommentAmount(Integer.parseInt(parser.nextText()));
                                break;
                        }

                        break;
                    case XmlPullParser.END_TAG:
                        if ("entry".equals(parser.getName())) {
                            newsList.add(newsEntity);
                            newsEntity = null;
                        }
                        break;
                }
                type = parser.next();
            }
        } catch (Exception e) {
            return null;
        }
        return newsList;
    }

    public long getNewsID() {
        return newsID;
    }

    public void setNewsID(long newsID) {
        this.newsID = newsID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public int getRecommendAmount() {
        return recommendAmount;
    }

    public void setRecommendAmount(int recommendAmount) {
        this.recommendAmount = recommendAmount;
    }

    public int getCommentAmount() {
        return commentAmount;
    }

    public void setCommentAmount(int commentAmount) {
        this.commentAmount = commentAmount;
    }
}
